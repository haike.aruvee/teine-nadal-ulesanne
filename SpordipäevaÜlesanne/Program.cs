﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpordipäevaÜlesanne
{
    struct ProtokolliRida
    {
        public string Nimi;
        public int Distants;
        public double Aeg;
        public double Kiirus => Distants / Aeg; // see Kiirus on nn Calculated field (read only property)
        //public override string ToString()
        
    }



    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\protokoll.txt";
           
            var protokoll = System.IO.File.ReadAllLines(filename) //annab stringi massiivi
                .Skip(1) //st., et esimene rida jäetakse vahele ja annab IEnumerable stringi
                .Where(x => x.Trim() != "") //jätab tühjad read vahele
                .Select(x => x.Split(',').Select(y => y.Trim()).ToArray()) //x-st tulevad stringi massiivid ja peale splittimist Selectiga tehakse IEnumerabli, mis on trimmitud ja sellest tehakse Array. y sellepärast, et x on juba selles funktsioonis parameetrina kasutusel
                .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]) })
                .Select(x=> new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants / x.Aeg}) //anonüümne tüüp mis koosneb nimest, distantsist ja ajast
                .ToList(); //teeb anonüümsest struktuurist uue anonüümse struktuuri
            protokoll.ForEach(x => Console.WriteLine(x));

            //kes on kõige kiirem jooksja

            Console.WriteLine(protokoll.OrderByDescending(x => x.Kiirus)
                .Take(1) //võtab esimese ja see loogika, et sellepärast ainult esimene, kuna teisi pole vaja sama tulemuse annab ka First()
                .Select(x => $"Kõige kiirem jooksja on {x.Nimi} tema kiirus on {x.Kiirus}") //sisse tuleb string, välja arv ehk funktsioon, mida teha sisse tulnud asjadega
                //üldiselt sisse ja välja IEnumerable aga kui pole kogu kogumit vaja siis kasutada Single
                .Single()); //see annab ainukese aga kui on rohkem hulgas siis see ei tööta. Samuti kui pole ühtegi asja valimis - siis Single ja First annavad vea.
                            //siis tuleb appi FirstOrDefault() või SingleOrDefault()

            //kes on kõige kiirem sprinter

            Console.WriteLine(protokoll
                .Where(x => x.Distants == 100)
                //.OrderBy(x => - x.Kiirus)
                .OrderByDescending(x => x.Kiirus) //paneb suuruse järjekorda
                .Take(1)
                .Select(x => $"\nKõige kiirem sprinter on {x.Nimi} ja tema kiirus on {x.Distants} meetril on ")
                .Single()
                );
        }
    }
}
