﻿Tuletame meelde pisukese vana

lisa oma programmi algusse:
using System.IO;

siis tekkib sul võimalus kasutada klassi File

string failiNimi = "siiafailinimi.txt";
string[] loetudRead = File.ReadAllLines(failiNimi);

või (võid ka nii alustada!)
var protokoll = File.ReadAllLines(failiNimi)
		.Where(x => x.Trim() != "")
		.Select(x => x.Split(',').Select(y => y.Trim()))
		.Select(x => new {Nimi = x[0], ... })
		.veelmidagi
		.ToArray(); // või taolist?

Ülesanne:
meil on spordipäeva protokoll (leiad meie salvestuste kaustast)
Nimi, distants, aeg
leia kõige kiirem sprinter
kõige kiirem jooksja (mis neil vahet)

ja kellel aega, leiab kõige stabiilsema jooksja
* osalenud vähemalt kahel distantsil
* kiiruste erinevus distantsidel kõige väiksem